// home.controller.js
(() => {

    angular
        .module('app')
        .controller('HomeController', HomeController);

    function HomeController($location, $http, apiBase, OTSession) {
        const vm = this;
        vm.header = 'Teste Opentok!';
        vm.$onInit = onInit;
        vm.streams
        vm.seasonId;
        vm.apiKey;
        vm.token;
        vm.archiveID;
        vm.sessionIsOpen = false;
        vm.sessionIsRecording = false;
        vm.linkShareable;

        activate();

        vm.iniciarFecharSessao = function () {
            vm.sessionIsOpen ? fecharSessao() : abrirSessao();
        };
        
        function abrirSessao() {           
            $http.get("http://localhost:8080/sessao/")
            .then( function (res) {
                console.log(res.data);
                vm.sessionId = res.data.sessionId;
                vm.apiKey = res.data.apiKey;
                vm.token = res.data.token;
                vm.sessionIsOpen = true;
                vm.linkShareable= `http://localhost:8080/${vm.sessionId}`;

                
                var session = OT.initSession(vm.apiKey, vm.sessionId);
                    // Subscribe to a newly created stream
                    session.on('streamCreated', function streamCreated(event) {
                        var subscriberOptions = {
                        insertMode: 'append',
                        width: '100%',
                        height: '100%'
                        };
                        session.subscribe(event.stream, 'subscriber', subscriberOptions, function callback(error) {
                        if (error) {
                            console.log('There was an error publishing: ', error.name, error.message);
                        }
                        });
                    });
                    
                    session.on('archiveStarted', function archiveStarted(event) {
                        vm.archiveID = event.id;
                        console.log('Archive started ' + vm.archiveID);
                    });
                    
                    session.on('archiveStopped', function archiveStopped(event) {
                        vm.archiveID = event.id;
                        console.log('Archive stopped ' + vm.archiveID);
                    });
                    
                    session.on('sessionDisconnected', function sessionDisconnected(event) {
                        console.log('You were disconnected from the session.', event.reason);
                    });

                    // Connect to the session
                    session.connect(vm.token, function connectCallback(error) {
                        // If the connection is successful, initialize a publisher and publish to the session
                        if (!error) {
                        var publisherOptions = {
                            insertMode: 'append',
                            width: '100%',
                            height: '100%'
                        };
                        var publisher = OT.initPublisher('publisher', publisherOptions, function initCallback(err) {
                            if (err) {
                            console.log('There was an error initializing the publisher: ', err.name, err.message);
                            return;
                            }
                            session.publish(publisher, function publishCallback(pubErr) {
                            if (pubErr) {
                                console.log('There was an error publishing: ', pubErr.name, pubErr.message);
                            }
                            });
                        });
                        } else {
                        console.log('There was an error connecting to the session: ', error.name, error.message);
                        }
                    });
                


            }).catch( function (err) {
                console.log(err);
            });
        }

        function fecharSessao() {
            session.off('sessionOffEvent', (event) =>{
                console.log(event);
            });

            console.log("Fecha sessão");
        }

        vm.iniciarPararGravacao = function () {
            vm.sessionIsRecording ? pararGravacao() : iniciarGravacao();
        };

        function iniciarGravacao() {
            $http.post("http://localhost:8080/sessao/start",
            {
                'sessionId': vm.sessionId,
                'apiKey': vm.apiKey,
                'token': vm.token
            })
            .then( function (res) {
                console.log(res.data);
                vm.sessionIsRecording = true;
            }).catch( function (err) {
                console.log(err);
            });
        }

        function pararGravacao() {
            $http.post(`http://localhost:8080/sessao/${vm.archiveID}/stop`)
            .then( function (res) {
                console.log(res.data);
                vm.sessionIsRecording = false;
                vm.sessionIsOpen = false;

                session.off('sessionOffEvent', (event) =>{
                    console.log(event);
                });

            }).catch( function (err) {
                console.log(err);
            });
        }

        // Handling all of our errors here by alerting them
        function handleError(error) {
            if (error) {
            alert(error.message);
            }
        }

        function initializeSession() {
            var session = OT.initSession(vm.apiKey, vm.sessionId);
        
            // Subscribe to a newly created stream
            session.on('streamCreated', function(event) {
                session.subscribe(event.stream, 'subscriber', {
                insertMode: 'append',
                width: '100%',
                height: '100%'
                }, handleError);
            });
        
            // Create a publisher
            var publisher = OT.initPublisher('publisher', {
            insertMode: 'append',
            width: '100%',
            height: '100%'
            }, handleError);
        
            // Connect to the session
            session.connect(token, function(error) {
                // If the connection is successful, publish to the session
                if (error) {
                    handleError(error);
                } else {
                    session.publish(publisher, handleError);
                }
            });

            // vm.sessionIsOpen = session.currentState === 'connected';
        }

        function activate() {
            
        }

        function onInit() {
        }
    }

})();