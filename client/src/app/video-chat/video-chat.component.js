// video-chat.component.js
(() => {

    angular
        .module('app')
        .component('videoChat', {
            controller: 'VideoChatController',
            controllerAs: 'vm',
            templateUrl: 'app/video-chat/video-chat.html'
    });

})();