// video-chat.controller.js
(() => {
  angular.module("app").controller("VideoChatController", VideoChatController);

  function VideoChatController($location, $http, apiBase, OTSession) {
    const vm = this;
    vm.$onInit = onInit;
    vm.streams;
    vm.session;
    vm.seasonId;
    vm.apiKey;
    vm.token;
    vm.archiveID;
    vm.sessionIsOpen = false;
    vm.sessionIsRecording = false;
    vm.linkShareable;
    vm.muteMicrophone = muteMicrophone;
    vm.pauseVideo = pauseVideo;
    vm.recordVideo = pauseVideo;
    vm.test = test;
    // vm.windowResized = windowResized;
    // vm.log = logIt;

    vm.iniciarFecharSessao = function () {
      vm.sessionIsOpen ? fecharSessao() : abrirSessao();
    };

    function abrirSessao() {
      $http
        .get("http://localhost:8080/sessao/")
        .then(function (res) {
          console.log(res.data);
          vm.sessionId = res.data.sessionId;
          vm.apiKey = res.data.apiKey;
          vm.token = res.data.token;
          vm.sessionIsOpen = true;
          vm.linkShareable = `http://localhost:8080/${vm.sessionId}`;

          var session = OT.initSession(vm.apiKey, vm.sessionId);
          // Subscribe to a newly created stream
          session.on("streamCreated", function streamCreated(event) {
            console.log(`🚩 Sessão on!`);
            var subscriberOptions = {
              insertMode: "append",
              width: "100%",
              height: "100%",
            };
            session.subscribe(
              event.stream,
              "remote-video",
              subscriberOptions,
              function callback(error) {
                console.log(`🚩 Tentou increver!`);
                if (error) {
                  console.log(
                    "There was an error publishing: ",
                    error.name,
                    error.message
                  );
                }
              }
            );

            // session.subscribe(event.stream, "remote-video", function callback(
            //   error
            // ) {
            //   if (error) {
            //     console.log(
            //       "There was an error publishing: ",
            //       error.name,
            //       error.message
            //     );
            //   }
            // });

            showLinkToShare();
          });

          session.on("archiveStarted", function archiveStarted(event) {
            vm.archiveID = event.id;
            console.log("Archive started " + vm.archiveID);
          });

          session.on("archiveStopped", function archiveStopped(event) {
            vm.archiveID = event.id;
            console.log("Archive stopped " + vm.archiveID);
          });

          session.on("sessionDisconnected", function sessionDisconnected(
            event
          ) {
            console.log(
              "You were disconnected from the session.",
              event.reason
            );
          });

          // Connect to the session
          session.connect(vm.token, function connectCallback(error) {
            console.log(`🚩 Sessão connect!`);
            // If the connection is successful, initialize a publisher and publish to the session
            if (!error) {
              var publisherOptions = {
                insertMode: "append",
                width: "100%",
                height: "100%",
              };
              var publisher = OT.initPublisher(
                "local-video",
                publisherOptions,
                function initCallback(err) {
                  if (err) {
                    console.log(
                      "There was an error initializing the publisher: ",
                      err.name,
                      err.message
                    );
                    return;
                  }
                  session.publish(publisher, function publishCallback(pubErr) {
                    if (pubErr) {
                      console.log(
                        "There was an error publishing: ",
                        pubErr.name,
                        pubErr.message
                      );
                    }
                  });
                }
              );
            } else {
              console.log(
                "There was an error connecting to the session: ",
                error.name,
                error.message
              );
            }
          });

          vm.session = session;
        })
        .catch(function (err) {
          console.log(err);
        });
    }

    function fecharSessao() {
      session.off("sessionOffEvent", (event) => {
        console.log(event);
      });

      console.log("Fecha sessão");
    }

    vm.iniciarPararGravacao = function () {
      vm.sessionIsRecording ? pararGravacao() : iniciarGravacao();
    };

    function iniciarGravacao() {
      $http
        .post("http://localhost:8080/sessao/start", {
          sessionId: vm.sessionId,
          apiKey: vm.apiKey,
          token: vm.token,
        })
        .then(function (res) {
          console.log(res.data);
          vm.sessionIsRecording = true;
        })
        .catch(function (err) {
          console.log(err);
        });
    }

    function pararGravacao() {
      $http
        .post(`http://localhost:8080/sessao/${vm.archiveID}/stop`)
        .then(function (res) {
          console.log(res.data);
          vm.sessionIsRecording = false;
          vm.sessionIsOpen = false;

          session.off("sessionOffEvent", (event) => {
            console.log(event);
          });
        })
        .catch(function (err) {
          console.log(err);
        });
    }

    // Handling all of our errors here by alerting them
    function handleError(error) {
      if (error) {
        alert(error.message);
      }
    }

    function initializeSession() {
      var session = OT.initSession(vm.apiKey, vm.sessionId);

      // Subscribe to a newly created stream
      session.on("streamCreated", function (event) {
        session.subscribe(
          event.stream,
          "remote-video",
          {
            insertMode: "append",
            width: "100%",
            height: "100%",
          },
          handleError
        );
      });

      // Create a publisher
      var publisher = OT.initPublisher(
        "local-video",
        {
          insertMode: "append",
          width: "100%",
          height: "100%",
        },
        handleError
      );

      // Connect to the session
      session.connect(token, function (error) {
        // If the connection is successful, publish to the session
        if (error) {
          handleError(error);
        } else {
          session.publish(publisher, handleError);
        }
      });

      // vm.sessionIsOpen = session.currentState === 'connected';
    }

    /////////////////////////////////////////

    vm.remoteVideo = $("#remote-video");
    vm.captionText = $("#remote-video-text");
    vm.localVideoText = $("#local-video-text");
    vm.captionButtontext = $("#caption-button-text");

    // var VideoChat = {
    //   connected: false,
    //   willInitiateCall: false,
    //   localICECandidates: [],
    //   remoteVideo: document.getElementById("remote-video"),
    //   localVideo: document.getElementById("local-video"),
    //   recognition: undefined,
    //   token: vm.token,
    // };

    function logIt(message, error) {
      console.log(`🚩 ${message}`);
    }

    function test() {
      if (!vm.session) {
        console.warn(`🚩 Nenhuma sessão foi iniciada!`);
      } else {
        console.log(vm.session);
        // vm.session.subscribe(stream, 'remote-video');
      }
    }

    function showLinkToShare() {
      console.log("🏁 showLinkToShare");
      Snackbar.show({
        text: `Compartilhar sessão: ${vm.linkShareable}`,
        actionText: "Copiar",
        width: "750px",
        pos: "top-center",
        actionTextColor: "#616161",
        duration: 500000,
        backgroundColor: "#16171a",
        onActionClick: function (element) {
          // Copy url to clipboard, this is achieved by creating a temporary element,
          // adding the text we want to that element, selecting it, then deleting it
          var copyContent = window.location.href;
          $('<input id="some-element">')
            .val(copyContent)
            .appendTo("body")
            .select();
          document.execCommand("copy");
          var toRemove = document.querySelector("#some-element");
          toRemove.parentNode.removeChild(toRemove);
          Snackbar.close();
        },
      });
    }

    function rePositionLocalVideo() {
      // Get position of remote video
      var bounds = vm.remoteVideo.position();
      let localVideo = $("#local-video");
      if (
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
          navigator.userAgent
        )
      ) {
        bounds.top = $(window).height() * 0.7;
        bounds.left += 10;
      } else {
        bounds.top += 10;
        bounds.left += 10;
      }
      // Set position of local video
      $("#moveable").css(bounds);
    }

    // Called when window is resized
    function windowResized() {
      rePositionLocalVideo();
      rePositionCaptions();
    }

    // Mute microphone
    function muteMicrophone() {}
    // End Mute microphone

    // Pause Video
    function pauseVideo() {}
    // End pause Video

    function startUp() {
      console.log("🏁 startUp");

      // Make local video draggable
      $("#moveable").draggable({
        containment: "window",
        create: (event, ui) => {
          console.log("🏁 create draggable\n" + event + "\n" + ui);
        },
      });

      // Hide button labels on load
      $(".HoverState").hide();

      // Show hide button labels on hover
      $(document).ready(function () {
        console.log("🏁 document ready");
        $(".hoverButton").mouseover(function () {
          $(".HoverState").hide();
          $(this).next().show();
        });
        $(".hoverButton").mouseout(function () {
          $(".HoverState").hide();
        });
      });

      // Fade out / show UI on mouse move
      var timedelay = 1;
      function delayCheck() {
        if (timedelay === 5) {
          // $(".multi-button").fadeOut();
          $("#header").fadeOut();
          timedelay = 1;
        }
        timedelay = timedelay + 1;
      }
      $(document).mousemove(function () {
        $(".multi-button").fadeIn();
        $("#header").fadeIn();
        $(".multi-button").style = "";
        timedelay = 1;
        clearInterval(_delay);
        _delay = setInterval(delayCheck, 500);
      });
      _delay = setInterval(delayCheck, 500);

      // Basic logging class wrapper
      function logIt(message, error) {
        console.log(message);
      }

      // On change media devices refresh page and switch to system default
      navigator.mediaDevices.ondevicechange = () => window.location.reload();
    }

    activate();

    function activate() {}

    function onInit() {
      console.log("🏁 onInit");
      startUp();
    }
  }
})();
