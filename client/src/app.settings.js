// app.settings.js
(() => {

    angular.module('app')

        .value('debuggingBorder', true)
        .value('apiBase', 'localhost:8080')

})();