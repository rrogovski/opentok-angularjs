package com.rrogovski.opentokapi.services;

import com.twilio.jwt.accesstoken.AccessToken;
import com.twilio.jwt.accesstoken.VideoGrant;

import org.springframework.stereotype.Service;

import lombok.Getter;

/**
 * TwilioService
 */
@Service
@Getter
public class TwilioService {

  private String twilioAccountSid = "AC91808acf1dec51b32b7199c15aff494d";
  private String twilioApiKey = "SK3876084fd49c7d3b9252787a97991900";
  private String twilioApiSecret = "WweAUQhWj6QDhWfVcYedeh4RUxnaCPb7";
  String identity = "jmj";

  public AccessToken token() {
    // Create Video grant
    VideoGrant grant = new VideoGrant();
    grant.setRoom("TwilioTeste");

    // Create access token
    AccessToken token = new AccessToken.Builder(twilioAccountSid, twilioApiKey, twilioApiSecret).identity(identity)
        .grant(grant).build();

    return token;
  }
}