package com.rrogovski.opentokapi.exceptions;

/**
 * FalhaIniciarSessao
 */
public class FalhaIniciarSessao extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public FalhaIniciarSessao(String mensagem) {
    super(mensagem);
  }

  public FalhaIniciarSessao(String mensagem, Throwable causa) {
    super(mensagem, causa);
  }
}