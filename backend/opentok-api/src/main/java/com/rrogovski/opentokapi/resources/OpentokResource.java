package com.rrogovski.opentokapi.resources;

import com.rrogovski.opentokapi.entities.SessaoOpentok;
import com.rrogovski.opentokapi.services.OpentokService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * OpentokResource
 */
@RestController
@RequestMapping("/sessao")
public class OpentokResource {

  @Autowired
  private OpentokService opentokService;

  // @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
  @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
  public ResponseEntity<SessaoOpentok> iniciarSessao() {
    return ResponseEntity.ok(opentokService.iniciarSessao());
  }

  @PostMapping("/start")
  public ResponseEntity<String> gravacao(@RequestBody SessaoOpentok sessao) {
    return ResponseEntity.ok(opentokService.iniciarGravacao(sessao.getSessionId()));
  }

  @PostMapping("/{arquivoId}/stop")
  public ResponseEntity<String> gravacao(@PathVariable("arquivoId") String arquivoId) {
    return ResponseEntity.ok(opentokService.pararGravacao(arquivoId));
  }

  // @PutMapping(value="/{id}/fechar")
  // public ResponseEntity<SessaoOpentok> fecharSessao(@PathVariable String id, @RequestBody ResponseEntity<SessaoOpentok> sessao) {
  //   return ResponseEntity.ok(opentokService.fecharSessao());
  // }
}