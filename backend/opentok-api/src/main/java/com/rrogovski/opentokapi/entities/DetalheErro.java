package com.rrogovski.opentokapi.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DetalheErro
 */
@Getter
@Setter
@NoArgsConstructor
public class DetalheErro {

  private String titulo;
  private Long status;
  private Long timestamp;
  private String mensagemDesenvolvedor;
}