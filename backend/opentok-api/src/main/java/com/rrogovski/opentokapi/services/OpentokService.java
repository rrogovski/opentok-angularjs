package com.rrogovski.opentokapi.services;

import java.time.LocalDateTime;

import com.opentok.Archive;
import com.opentok.ArchiveLayout;
import com.opentok.ArchiveMode;
import com.opentok.ArchiveProperties;
import com.opentok.MediaMode;
import com.opentok.OpenTok;
import com.opentok.Session;
import com.opentok.SessionProperties;
import com.opentok.Archive.OutputMode;
import com.opentok.exception.OpenTokException;
import com.rrogovski.opentokapi.entities.SessaoOpentok;
import com.rrogovski.opentokapi.exceptions.FalhaIniciarSessao;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;

import lombok.Getter;

/**
 * OpentokService
 */
@Service
public class OpentokService {

  @Getter
  private final String apiKey = "46653242"; // System.getProperty("API_KEY");
  private final String apiSecret = "92ab50bfdaeb033ff84e2e12d3d6626dad519830"; // System.getProperty("API_SECRET");
  private OpenTok opentok;
  private String sessionId;
  private String token;
  private String focusStreamId = "";
  private String layoutType = "horizontalPresentation";

  public String getSessionId() {

    if (this.sessionId == null || this.sessionId.isEmpty())
      iniciarSessao();

    return this.sessionId;
  }

  public String token() {

    if (this.sessionId == null || this.sessionId.isEmpty())
      throw new FalhaIniciarSessao("Sessão não foi iniciada!");

    return this.token;
  }

  public SessaoOpentok iniciarSessao() throws FalhaIniciarSessao {
    SessaoOpentok sessao = new SessaoOpentok();

    if (apiKey == null || apiKey.isEmpty() || apiSecret == null || apiSecret.isEmpty()) {
      System.out.println("Você deve definir as propriedades API_KEY e API_SECRET.");
    }

    opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);

    try {
      Session session = opentok.createSession(new SessionProperties.Builder().mediaMode(MediaMode.ROUTED)
          // .archiveMode(ArchiveMode.ALWAYS)
          .build());

      sessionId = session.getSessionId();
      token = session.generateToken();

    } catch (OpenTokException e) {
      throw new FalhaIniciarSessao("Falha ao inciar a sessão!");
    }

    sessao.setSessionId(sessionId);
    sessao.setApiKey(apiKey);
    sessao.setToken(token);

    // opentok.close();

    return sessao;
  }

  public String iniciarGravacao(String sessionId) {
    Archive arquivo = null;
    Boolean hasAudio = true, hasVideo = true;
    OutputMode outputMode = OutputMode.COMPOSED;
    ArchiveLayout layout = new ArchiveLayout(ArchiveLayout.Type.HORIZONTAL);

    try {
      ArchiveProperties properties = new ArchiveProperties.Builder()
          .name(String.format("TesteGravacao-%s", LocalDateTime.now().toString()))
          // .hasAudio(hasAudio)
          // .hasVideo(hasVideo)
          // .outputMode(outputMode)
          // .layout(layout)
          .build();
      arquivo = opentok.startArchive(sessionId, properties);
    } catch (OpenTokException e) {
      throw new FalhaIniciarSessao("Falha ao inciar a gravação!");
    }
    return arquivo.toString();
  }

  public String pararGravacao(String arquivoId) {
    Archive arquivo = null;

    try {
      arquivo = opentok.stopArchive(arquivoId);
      // opentok.forceDisconnect(sessionId, connectionId);
      opentok.close();
    } catch (OpenTokException e) {
      throw new FalhaIniciarSessao("Falha ao parar a gravação!");
    }
    return arquivo.toString();
  }

  // public String subscriber(String )

  // public SessaoOpentok fecharSessao(String sessionId) {
  // SessaoOpentok sessao = new SessaoOpentok();

  // opentok.
  // return sessao;
  // }
}