package com.rrogovski.opentokapi.handler;

import javax.servlet.http.HttpServletRequest;

import com.opentok.exception.OpenTokException;
import com.rrogovski.opentokapi.entities.DetalheErro;
import com.rrogovski.opentokapi.exceptions.FalhaIniciarSessao;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * ExceptionHandlerOpentok
 */
@ControllerAdvice
public class ExceptionHandlerOpentok {

  @ExceptionHandler(OpenTokException.class)
  public ResponseEntity<DetalheErro> handleFalhaIniciarSessao(FalhaIniciarSessao e, HttpServletRequest request) {

    DetalheErro erro = new DetalheErro();
    erro.setStatus(404l);
    erro.setTitulo("Falha ao iniciar a sessão!");
    erro.setMensagemDesenvolvedor(String.format("%s/404"));
    erro.setTimestamp(System.currentTimeMillis());

    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
  }
}