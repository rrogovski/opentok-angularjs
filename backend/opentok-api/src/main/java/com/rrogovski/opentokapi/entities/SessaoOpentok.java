package com.rrogovski.opentokapi.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * SessaoOpentok
 */
@Getter
@Setter
@NoArgsConstructor
public class SessaoOpentok {

  private String apiKey;
  private String sessionId;
  private String token;
}